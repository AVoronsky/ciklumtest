# Steps to integrate:
1. Add elogger module to your project.
2. Add module to settings.gradle. E.g.:
```
    include ':app', ':elogger'
```

3. Add dependency to your app module. E.g.:
```
    dependencies {
        ...

        implementation project(':elogger')

        ...
    }
```
    
To start work with library you have to init it in your onCreate() method of Application. E.g.:

```
class CiklumApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        
        // library initializing
        ELoggerInitializer.init(this)
    }
}
```

Now you are free to use it where you want - it will automatically catch all unhandled 
exceptions and you can easily to log your handled exceptions. E.g.:
```
    try {
        throw IllegalArgumentException()
    } catch (e: Exception) {
        ELoggerInitializer.getELogger().put(e)
    }
```
