package com.voronsky.ciklumtest

import android.app.Application
import com.voronsky.elogger.ELoggerInitializer

class CiklumApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        ELoggerInitializer.init(this)
    }
}