package com.voronsky.ciklumtest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.voronsky.elogger.ELoggerInitializer
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonCaughtException.setOnClickListener {
            try {
                throw IllegalArgumentException()
            } catch (e: Exception) {
                ELoggerInitializer.getELogger().put(e)
            }
        }
        buttonUncaughtException.setOnClickListener { throw RuntimeException() }
    }
}
