package com.voronsky.elogger.delivery

interface LogManager {

    fun addLog(eLoggerException: ELoggerException)

    fun addCrashLog(eLoggerException: ELoggerException)
}