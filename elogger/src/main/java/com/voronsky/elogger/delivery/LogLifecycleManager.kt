package com.voronsky.elogger.delivery

import android.util.Log
import com.voronsky.elogger.common.Worker
import com.voronsky.elogger.data.db.LocalRepository
import com.voronsky.elogger.data.entity.ExceptionDto
import com.voronsky.elogger.data.remote.RemoteRepository
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class LogLifecycleManager(private val localRepository: LocalRepository,
                          private val remoteRepository: RemoteRepository,
                          private val worker: Worker) : LogManager {

    private companion object {
        const val TAG = "LogLifecycleManager"
        const val INITIAL_DELAY = 0L
        const val INTERVAL_TIME = 10L
    }

    init {
        Observable.interval(INITIAL_DELAY, INTERVAL_TIME, TimeUnit.MINUTES)
                .flatMap { localRepository.getExceptions() }
                .filter { it.isNotEmpty() }
                .flatMap { e ->
                    remoteRepository.sendLogs(e)
                            .toObservable<Void>()
                            .map { localRepository.delete(e) }
                }
                .retryWhen { it.delay(INTERVAL_TIME, TimeUnit.MINUTES) }
                .subscribeOn(worker.ioScheduler)
                .subscribe({ Log.e(TAG, "Logs are delivered") },
                        { Log.e(TAG, "Error while delivering logs") })
    }

    override fun addLog(eLoggerException: ELoggerException) {
        log(eLoggerException, worker.ioScheduler)
                .subscribe({ Log.e(TAG, "Exception was added") },
                        { Log.e(TAG, "Exception wasn't added because of\n$it") })
    }

    override fun addCrashLog(eLoggerException: ELoggerException) {
        log(eLoggerException, worker.singleThreadScheduler)
                .subscribe({
                    Log.e(TAG, "Crash exception was added")
                    System.exit(-1)
                }, { Log.e(TAG, "Crash exception wasn't added because of\n$it") })
    }

    private fun log(eLoggerException: ELoggerException, scheduler: Scheduler): Single<Unit> {
        return Single.fromCallable { localRepository.insertException(mapELoggerExceptions(eLoggerException)) }
                .subscribeOn(scheduler)
    }

    private fun mapELoggerExceptions(eLoggerException: ELoggerException): ExceptionDto {
        return ExceptionDto(eLoggerException.timestamp,
                eLoggerException.exception,
                eLoggerException.comment,
                eLoggerException.tag,
                eLoggerException.identifier,
                eLoggerException.deviceName,
                eLoggerException.osVersion,
                eLoggerException.freeSpace,
                eLoggerException.freeRam,
                eLoggerException.batteryLevel)
    }
}
