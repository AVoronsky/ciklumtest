package com.voronsky.elogger.delivery

data class ELoggerException(val timestamp: Long,
                            val exception: String,
                            val comment: String,
                            val tag: String,
                            val identifier: String,
                            val deviceName: String,
                            val osVersion: String,
                            val freeSpace: Long,
                            val freeRam: Long,
                            val batteryLevel: Float)