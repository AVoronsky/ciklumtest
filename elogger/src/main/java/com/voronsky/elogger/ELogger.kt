package com.voronsky.elogger

interface ELogger {

    /**
     * Saves exception to repository
     *
     * @param thr - exception to save
     */
    fun put(thr: Throwable)

    /**
     * Saves exception to repository with additional comment
     *
     * @param comment - comment that can provide for e.g. some additional explanation
     * @param thr - exception to save
     */
    fun put(comment: String, thr: Throwable)

    /**
     * Saves exception to repository with additional comment with some tag to provide filter ability
     *
     * @param tag - specific tag for exception
     * @param comment - comment that can provide for e.g. some additional explanation
     * @param thr - exception to save
     */
    fun put(tag: String, comment: String, thr: Throwable)

    /**
     * To add some custom identifier to app instance. E.g. IMEI
     *
     * @param identifier - custom instance identifier
     */
    fun addIdentifier(identifier: String)
}