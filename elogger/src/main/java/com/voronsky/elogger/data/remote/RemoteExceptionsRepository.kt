package com.voronsky.elogger.data.remote

import com.voronsky.elogger.data.entity.ExceptionDto
import io.reactivex.Completable

class RemoteExceptionsRepository(private val postExceptionsService: PostExceptionsService)
    : RemoteRepository {

    override fun sendLogs(exceptions: List<ExceptionDto>): Completable {
        return postExceptionsService.sendExceptions(exceptions)
    }
}