package com.voronsky.elogger.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.voronsky.elogger.data.entity.ExceptionDto

@Database(entities = [ExceptionDto::class], version = DB_VERSION, exportSchema = false)
abstract class ExceptionsDb : RoomDatabase() {

    abstract fun exceptionsDao(): ExceptionsDao
}