package com.voronsky.elogger.data.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.voronsky.elogger.data.db.EXCEPTIONS_TABLE

@Entity(tableName = EXCEPTIONS_TABLE)
data class ExceptionDto(@PrimaryKey val timestamp: Long,
                        val exception: String,
                        val comment: String,
                        val tag: String,
                        val identifier: String,
                        val deviceName: String,
                        val osVersion: String,
                        val freeSpace: Long,
                        val freeRam: Long,
                        val batteryLevel: Float)