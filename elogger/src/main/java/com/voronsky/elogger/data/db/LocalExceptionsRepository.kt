package com.voronsky.elogger.data.db

import com.voronsky.elogger.data.entity.ExceptionDto
import io.reactivex.Observable

class LocalExceptionsRepository(private val db: ExceptionsDb) : LocalRepository {

    override fun getExceptions(): Observable<List<ExceptionDto>> = Observable.fromCallable { db.exceptionsDao().getExceptions() }

    override fun insertException(exception: ExceptionDto) = db.exceptionsDao().insertException(exception)

    override fun delete(exceptions: List<ExceptionDto>) = db.exceptionsDao().delete(exceptions)

    override fun deleteAll() = db.exceptionsDao().deleteAll()
}