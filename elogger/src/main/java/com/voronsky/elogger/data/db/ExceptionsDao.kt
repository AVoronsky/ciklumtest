package com.voronsky.elogger.data.db

import android.arch.persistence.room.*
import com.voronsky.elogger.data.entity.ExceptionDto

@Dao
interface ExceptionsDao {

    @Query("SELECT * FROM $EXCEPTIONS_TABLE")
    fun getExceptions(): List<ExceptionDto>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertException(exception: ExceptionDto)

    @Delete
    fun delete(exceptions: List<ExceptionDto>)

    @Query("DELETE FROM $EXCEPTIONS_TABLE")
    fun deleteAll()
}