package com.voronsky.elogger.data.db

import com.voronsky.elogger.data.entity.ExceptionDto
import io.reactivex.Observable

interface LocalRepository {

    /**
     * Gets needed count of exceptions if such is present otherwise returns all available
     *
     * @see ExceptionDto
     *
     * @return - list of exceptions
     */
    fun getExceptions(): Observable<List<ExceptionDto>>

    /**
     * Inserts new exception object to DB
     */
    fun insertException(exception: ExceptionDto)

    /**
     * Deletes specified exceptions from DB
     */
    fun delete(exceptions: List<ExceptionDto>)

    /**
     * Clears all data from DB
     */
    fun deleteAll()
}