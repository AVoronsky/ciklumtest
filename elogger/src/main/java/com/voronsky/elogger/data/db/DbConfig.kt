package com.voronsky.elogger.data.db

const val DB_VERSION = 1
const val DB_NAME = "exceptions_db"

const val EXCEPTIONS_TABLE = "EXCEPTIONS_TABLE"