package com.voronsky.elogger.data.remote

import com.voronsky.elogger.data.entity.ExceptionDto
import io.reactivex.Completable

interface RemoteRepository {

    /**
     * Sends a list of exceptions to the server
     *
     * @param exceptions - a list of exceptions
     * @see ExceptionDto
     *
     * @return - returns a success if delivered or an error if failed
     */
    fun sendLogs(exceptions: List<ExceptionDto>): Completable
}