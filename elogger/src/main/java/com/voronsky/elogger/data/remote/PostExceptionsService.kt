package com.voronsky.elogger.data.remote

import com.voronsky.elogger.data.entity.ExceptionDto
import io.reactivex.Completable
import retrofit2.http.Body
import retrofit2.http.POST

interface PostExceptionsService {

    @POST("/exceptions")
    fun sendExceptions(@Body exceptions: List<ExceptionDto>): Completable
}