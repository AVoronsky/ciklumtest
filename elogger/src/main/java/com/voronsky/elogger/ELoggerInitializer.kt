package com.voronsky.elogger

import android.arch.persistence.room.Room
import android.content.Context
import com.google.gson.GsonBuilder
import com.voronsky.elogger.common.*
import com.voronsky.elogger.data.db.DB_NAME
import com.voronsky.elogger.data.db.ExceptionsDb
import com.voronsky.elogger.data.db.LocalExceptionsRepository
import com.voronsky.elogger.data.db.LocalRepository
import com.voronsky.elogger.data.remote.PostExceptionsService
import com.voronsky.elogger.data.remote.RemoteExceptionsRepository
import com.voronsky.elogger.data.remote.RemoteRepository
import com.voronsky.elogger.delivery.ELoggerException
import com.voronsky.elogger.delivery.LogLifecycleManager
import com.voronsky.elogger.delivery.LogManager
import io.reactivex.schedulers.Schedulers


object ELoggerInitializer {

    @Volatile
    private var logger: ELogger? = null

    fun init(context: Context) {
        logger = ExceptionLogger(context)
    }

    @Synchronized
    fun getELogger(): ELogger = logger!!

    private class ExceptionLogger(private var context: Context) : ELogger {

        private val logManager: LogManager
        private var identifier: String = ""

        init {
            logManager = getLogManager(context)
            Thread.setDefaultUncaughtExceptionHandler { _, e ->
                run {
                    collectException(throwable = e, isCrash = true)
                }
            }
        }

        override fun put(thr: Throwable) {
            collectException(throwable = thr)
        }

        override fun put(comment: String, thr: Throwable) {
            collectException(comment = comment, throwable = thr)
        }

        override fun put(tag: String, comment: String, thr: Throwable) {
            collectException(tag, comment, thr)
        }

        override fun addIdentifier(identifier: String) {
            this.identifier = identifier
        }

        private fun collectException(tag: String = "", comment: String = "", throwable: Throwable,
                                     isCrash: Boolean = false) {
            val exception = ELoggerException(System.currentTimeMillis(), throwable.stackTrace.toString(),
                    comment, tag, identifier, getDeviceName(), android.os.Build.VERSION.RELEASE,
                    getAvailableStorageInMb(), getAvailableRam(context), getBatteryPercentage(context))

            if (isCrash) {
                logManager.addCrashLog(exception)
            } else {
                logManager.addLog(exception)
            }
        }

        private fun getLogManager(context: Context): LogManager {
            return LogLifecycleManager(getLocalRepository(context), getRemoteRepository(), getWorkers())
        }

        private fun getLocalRepository(context: Context): LocalRepository {
            val db = Room.databaseBuilder(context, ExceptionsDb::class.java, DB_NAME).build()
            return LocalExceptionsRepository(db)
        }

        private fun getRemoteRepository(): RemoteRepository {
            val service = RestServiceBuilder(GsonBuilder())
                    .create(PostExceptionsService::class.java, BuildConfig.SERVER_URL)
            return RemoteExceptionsRepository(service)
        }

        private fun getWorkers(): Worker = Worker(Schedulers.io(), Schedulers.single())
    }
}