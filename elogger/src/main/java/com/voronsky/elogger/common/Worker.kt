package com.voronsky.elogger.common

import io.reactivex.Scheduler

data class Worker(val ioScheduler: Scheduler, val singleThreadScheduler: Scheduler)