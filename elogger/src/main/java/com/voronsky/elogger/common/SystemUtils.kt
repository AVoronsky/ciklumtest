package com.voronsky.elogger.common

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build
import android.os.Environment
import android.os.StatFs


private const val SIZE_KB = 1024L

fun getDeviceName(): String {
    val manufacturer = Build.MANUFACTURER
    val model = Build.MODEL
    return "$manufacturer $model"
}

fun getAvailableStorageInMb(): Long {
    val sizeMb = SIZE_KB * SIZE_KB
    val stat = StatFs(Environment.getExternalStorageDirectory().path)
    val availableSpace = stat.availableBlocksLong * stat.blockSizeLong
    return availableSpace / sizeMb
}

fun getAvailableRam(context: Context): Long {
    val mi = ActivityManager.MemoryInfo()
    val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    activityManager.getMemoryInfo(mi)

    return mi.availMem / 1048576L
}

fun getBatteryPercentage(context: Context): Float {
    val iFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
    val batteryStatus = context.registerReceiver(null, iFilter)

    val level = batteryStatus?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) ?: -1
    val scale = batteryStatus?.getIntExtra(BatteryManager.EXTRA_SCALE, -1) ?: -1

    val batteryPct = level / scale.toFloat()

    return batteryPct * 100
}