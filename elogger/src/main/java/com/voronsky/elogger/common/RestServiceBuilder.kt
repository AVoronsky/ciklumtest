package com.voronsky.elogger.common

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RestServiceBuilder(private val gsonBuilder: GsonBuilder) {

    private val okHttpClient: OkHttpClient

    init {
        okHttpClient = getHttpClientBuilder().build()
    }

    fun <T> create(serviceClass: Class<T>, baseUrl: String): T {
        return create(serviceClass, okHttpClient, baseUrl)
    }

    private fun <T> create(serviceClass: Class<T>, okHttpClient: OkHttpClient,
                           baseUrl: String): T {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        val retrofit = builder.client(okHttpClient).build()
        return retrofit.create(serviceClass)
    }

    private fun getHttpClientBuilder(): OkHttpClient.Builder {
        val logger = HttpLoggingInterceptor()
        logger.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
                .addInterceptor(logger)
    }
}